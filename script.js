const myArr = [1, 2, 3, true, 'hello', undefined];

function filterBy(arr, dataType) {
    const newArr = arr.filter((el) => {
        if (typeof el !== dataType) {
            return true;
        }
    });
    console.log(arr);
    console.log(newArr);
}

filterBy(myArr, 'number');

/*
Цикл forEach проходит по каджому элементу массива и выполняет для него callback функцию.
 */